import React from "react"
import MadeBy from "../MadeBy/index"
// import {ExternalLink, InternalLink} from "../Links"

const Footer = () => (
  <footer>
    <div className="left"></div>
    <div className="center"></div>
    <div className="right"></div>
    <MadeBy />
  </footer>
)

export default Footer
